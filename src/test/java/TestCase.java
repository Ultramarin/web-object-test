import io.restassured.response.Response;
import org.junit.Before;
import org.junit.Test;

import static io.restassured.RestAssured.*;
import static io.restassured.matcher.RestAssuredMatchers.*;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertEquals;

public class TestCase {

    public static final String BASE_URL = "http://www.omdbapi.com?apikey=cfdc15b1";//урл ключ api cfdc15b1

    @Test
    public void testCase1() {
        given()
                .param("s", "Rambo").
                when()
                .get(BASE_URL).
                then()
                .statusCode(200)
                .body("Search.findAll{it.Type == 'movie'}.size()", equalTo(5));
    }

    @Test
    public void testCase2() {
        Response firstResponse = given().param("t", "Ready Player One").get(BASE_URL);
        String firstResponseBody = firstResponse.asString();
        String imdbID = firstResponse.path("imdbID");
        Response secondResconse = given().param("i", imdbID).get(BASE_URL);
        String secondResponseBody = secondResconse.asString();
        assertEquals(firstResponseBody, secondResponseBody);
    }

}
